import requests
import mysql.connector
from bs4 import BeautifulSoup
from datetime import date
import time
from bs4 import Comment
import json 

#Read db connection JSON file
  
# Opening JSON file 
f = open('Excludes/db_connection.json',)
# returns JSON object as  
# a dictionary 
data = json.load(f) 
# Closing file 
f.close() 

mydb = mysql.connector.connect(
    host=data['db_server'],
    user=data['db_user'],
    passwd=data['db_passwd'],
    database=data['db_database'],
    auth_plugin=data['db_auth_plugin'],
)


def calculateAge(born): 
    today = date.today() 
    try:  
        birthday = born.replace(year = today.year) 
  
    # raised when birth date is February 29 
    # and the current year is not a leap year 
    except ValueError:  
        birthday = born.replace(year = today.year, 
                  month = born.month + 1, day = 1) 
  
    if birthday > today: 
        return today.year - born.year - 1
    else: 
        return today.year - born.year 
          


          #URL to be scraped

def populate_coach_college():
    mycursor = mydb.cursor()
    mycursor.execute("select * from list_coach where CoachCollegeID is NULL")
    coaches = mycursor.fetchall()
    mycursor.close()
    for coach in coaches:
        print(coach)
        #Load html's plain data into a variable
        plain_html_text = requests.get(coach[9])
        #parse the data
        soup = BeautifulSoup(plain_html_text.text, "html.parser")
        trs = soup.find_all('table',{'class' : 'infobox vcard'})[0].tbody.find_all('tr')
        time.sleep(5)
        for tr in trs:
            if ('College:' in tr.text):
                CollegeCommonName = (tr.text).replace("College:", "")
                CoachName = str(coach[1])
                CoachID = str(coach[0])
                print(CollegeCommonName, CoachName, CoachID)
                mycursor = mydb.cursor()
                sql = "UPDATE NFL.list_coach SET CoachCollegeID = (Select CollegeID from list_college WHERE CollegeCommonName = %s) WHERE CoachID = %s"
                val = (CollegeCommonName, CoachID)
                mycursor.execute(sql, val)
                mydb.commit()
                mycursor.close()

def populate_pfr_url():
    #https://www.pro-football-reference.com/
    # #GET Player, Coach, Executive URL's
    mycursor = mydb.cursor()
    mycursor.execute("select * from list_coach where CoachPFRPlayerURL is NULL AND CoachPFRCoachURL is NULL AND CoachPFRExecutiveURL is NULL")
    coaches = mycursor.fetchall()
    mycursor.close()
    PlayerURL = None
    CoachURL = None
    ExecutiveURL = None
    for coach in coaches:
        url_to_scrape = 'https://www.pro-football-reference.com/search/search.fcgi?search='+coach[2]+'+'+coach[3]
        #Load html's plain data into a variable
        plain_html_text = requests.get(url_to_scrape)
        soup = BeautifulSoup(plain_html_text.text, "html.parser")
        body = soup.body
        divs = body.find_all("div", {'class' : 'search-results'})
        redirect_url = plain_html_text.url
        #not redirected to search results page - coach was only 1 of the following: coach, player, executive
        if (divs == []):
            #coach exists on PFR
            if ("search" not in redirect_url):
                if '/players/' in redirect_url:
                    PlayerURL = redirect_url
                if '/coaches/' in redirect_url:
                    CoachURL = redirect_url
                if '/executives/' in redirect_url:
                    ExecutiveURL = redirect_url
        #redirected to search results page coach was more than 1 of the following: coach, player, executive
        else:
            #ensure search is in redirect URL
            if("search" in (plain_html_text.url).split("/")[4]):
                #find search results links
                links=soup.body.find_all("div", {'class' : 'search-results'})[0].find_all("a")
                for link in links:
                    #if specific link exists, do the needful
                    if '/players/' in link['href']:
                        PlayerURL = "https://www.pro-football-reference.com"+link['href']
                    if '/coaches/' in link['href']:
                        CoachURL = "https://www.pro-football-reference.com"+link['href']
                    if '/executives/' in link['href']:
                        ExecutiveURL = "https://www.pro-football-reference.com"+link['href']
        CoachID = str(coach[0])
        if (PlayerURL != None):
            mycursor = mydb.cursor()
            sql = "UPDATE NFL.list_coach SET CoachPFRPlayerURL = %s WHERE CoachID = %s"
            val = (PlayerURL, CoachID)
            mycursor.execute(sql, val)
            mydb.commit()
            mycursor.close()
            PlayerURL = None
        if (CoachURL != None):
            mycursor = mydb.cursor()
            sql = "UPDATE NFL.list_coach SET CoachPFRCoachURL = %s WHERE CoachID = %s"
            val = (CoachURL, CoachID)
            mycursor.execute(sql, val)
            mydb.commit()
            mycursor.close()
            CoachURL = None
        if (ExecutiveURL != None):
            mycursor = mydb.cursor()
            sql = "UPDATE NFL.list_coach SET CoachPFRExecutiveURL = %s WHERE CoachID = %s"
            val = (ExecutiveURL, CoachID)
            mycursor.execute(sql, val)
            mydb.commit()
            mycursor.close()
            ExecutiveURL = None
    
def populate_coach_entry():
    #https://www.pro-football-reference.com/
    mycursor = mydb.cursor()
    mycursor.execute("select * from list_coach where CoachEntryYear is NULL AND CoachPFRCoachURL IS NOT NULL")
    coaches = mycursor.fetchall()
    mycursor.close()
    for coach in coaches:
        CoachID = str(coach[0])
        #Load html's plain data into a variable
        plain_html_text = requests.get(coach[11])
        #parse the data
        commentsoup = BeautifulSoup(plain_html_text.text, "html.parser")
        comments = commentsoup.find_all(string=lambda text: isinstance(text, Comment))
        for comment in comments:
            if("Coaching History Table" in comment):
                soup = BeautifulSoup("<html>"+comment+"</html>", "html.parser")
                trs = soup.tbody.find_all("tr")
                ENTRYYEAR = trs[0].th.text
                mycursor = mydb.cursor()
                sql = "UPDATE NFL.list_coach SET CoachEntryYear = %s WHERE CoachID = %s"
                val = (ENTRYYEAR, CoachID)
                mycursor.execute(sql, val)
                mydb.commit()
                mycursor.close()

def correct_team_name(team):
    switcher={
            "Oakland Raiders":"Las Vegas Raiders",
            "St. Louis Rams" :"Los Angeles Rams",
            "San Diego Chargers": "Los Angeles Chargers",
            "Baltimore Colts":"Indianapolis Colts",
            "Houston Oilers":"Tennessee Titans",
            "Tennessee Oilers":"Tennessee Titans",
            "Los Angeles Raiders": "Las Vegas Raiders",
            "England Monarchs": "London Monarchs",
            "Florida Tuskers" : "Virginia Destroyers",
            "Orlando Renegades/Los Angeles Rams": "Orlando Renegades",
            "Montral Alouettes":"Montreal Alouettes",
            "Miami-FL":'Miami',
            "Connecticut":'UConn',
            "Massachusetts":'UMass',
            "USC":'Southern California',
            "Louisiana State":'LSU',
            "North Carolina State":'NC State',
            "Pitt":'Pittsburgh',
            "Miami (FL)":"Miami",
            "Northern Arizona University": "Northern Arizona",
            "Brigham Young":"BYU",
            "Troy State":"Troy",
            "Texas Christian":"TCU",
            "Central Connecticut State":"Central Connecticut",
            "Mississippi":"Ole Miss",
            "Catawba College":"Catawba",
            "Nevada-Las Vegas":"UNLV",
            "University of Utah":"Utah",
            "U.S. International":"Cal Poly",
            "Maine/Rutgers":"Rutgers",
            "Florida International":"FIU",
            "Virgina":"Virginia",
            "John Carroll (OH)":"John Carroll",
            "Defiance":"Defiance College",
            "Southeast Missouri State":"Southeast Missouri",
            "Rensselaer":"Rensselaer Polytechnic Institute",
            "Indiana (PA)":"IUP",
            "Western Maryland":"McDaniel College",
            "Grambling State":"Grambling",
            "Minnesota State Moorehead":"Minnesota State University Moorhead"
            }
    return switcher.get(team,team)

def correct_level_name(level):
    switcher={
            "College (FCS)":"College",
            "College (FBS)":"College",
            "College (NCAA D-II)": "College",
            "College (NCAA D-III)":"College",
            "WLAF":"NFL Europe",
            "College (NAIA)":"College",
            "6th Grade":"High School"
            }
    return switcher.get(level,level)

def populate_coach_jobs():
    #https://www.pro-football-reference.com/
    mycursor = mydb.cursor()
    mycursor.execute("select CoachID, CoachEntryYear, CoachPFRCoachURL from list_coach where CoachEntryYear is NOT NULL AND CoachPFRCoachURL IS NOT NULL")
    coaches = mycursor.fetchall()
    mycursor.close()
    for coach in coaches:
        CoachID = str(coach[0])
        CoachEntryYear = int(coach[1])
        #Load html's plain data into a variable
        plain_html_text = requests.get(coach[2])
        #parse the data
        commentsoup = BeautifulSoup(plain_html_text.text, "html.parser")
        comments = commentsoup.find_all(string=lambda text: isinstance(text, Comment))
        for comment in comments:
            if("Coaching History Table" in comment):
                soup = BeautifulSoup("<html>"+comment+"</html>", "html.parser")
                trs = soup.tbody.find_all("tr")
                for tr in trs:
                    Year = tr.th.text
                    tds = tr.find_all("td")
                    Level = tds[1].text
                    Level = correct_level_name(Level)
                    Team = tds[2].text
                    Team = correct_team_name(Team)
                    Team = Team.replace(";","")
                    Team = Team.replace("'","\\'")
                    Role = tds[3].text
                    Role = Role.replace("'","\\'")
                    if (Level == "High School"):
                        if(Role == ""):
                            Role = "Coach"
                    

                    #Check if Role exists in DB
                    mycursor = mydb.cursor()
                    sql = "select RoleID from list_team_role where RoleName = '"+Role+"'"
                    mycursor.execute(sql)
                    DBRole = mycursor.fetchall()
                    mycursor.close()
                    if (DBRole == []):
                        print(Role," Doesn't exist in DB")
                    else:
                        RoleID = DBRole[0][0]
                    DBRole = None

                    if(Level == "College"):
                        #Check if college Team exists in DB
                        mycursor = mydb.cursor()
                        sql = "select CollegeID from list_college where CollegeCommonName = '"+Team+"'"
                        mycursor.execute(sql)
                        DBTeam = mycursor.fetchall()
                        mydb.commit()
                        mycursor.close()
                        if (DBTeam == []):
                            print(Team," Doesn't exist in College DB")
                        else:
                            TeamID = DBTeam[0][0]
                        DBTeam = None
                    
                    if(Level == "NFL"):
                        #Check if NFL Team exists in DB
                        mycursor = mydb.cursor()
                        sql = "select TeamID from list_team where TeamName = '"+Team+"'"
                        mycursor.execute(sql)
                        DBTeam = mycursor.fetchall()
                        mydb.commit()
                        mycursor.close()
                        if (DBTeam == []):
                            print(Team," Doesn't exist in NFL DB")
                        else:
                            TeamID = DBTeam[0][0]
                        DBTeam = None
                    
                    if((Level == "NFL Europe")or(Level == "XFL")or(Level == "CFL")or(Level == "USFL")or(Level == "UFL")or(Level == "AAF")):
                        #Check if NFL Team exists in DB
                        mycursor = mydb.cursor()
                        sql = "select TeamID from list_team_other where TeamName = '"+Team+"'"
                        mycursor.execute(sql)
                        DBTeam = mycursor.fetchall()
                        mydb.commit()
                        mycursor.close()
                        if (DBTeam == []):
                            print(Team," Doesn't exist in other DB")
                        else:
                            TeamID = DBTeam[0][0]
                        DBTeam = None
                                        
                    #Check if level exists in DB
                    mycursor = mydb.cursor()
                    sql = "select LevelID from list_level where LevelName = '"+Level+"'"
                    mycursor.execute(sql)
                    DBLevel = mycursor.fetchall()
                    mydb.commit()
                    mycursor.close()
                    if (DBLevel == []):
                        print(Level," Doesn't exist in DB")
                    else:
                        LevelID = DBLevel[0][0]
                    DBLevel = None

                    if (Level == 'College'):
                        mycursor = mydb.cursor()
                        sql = "insert into relation_coach_job VALUES (%s,%s,%s,%s,(select CollegeID from list_college where CollegeCommonName=%s),0,0)"
                        val = (CoachID, Year, RoleID, LevelID, Team)
                        mycursor.execute(sql, val)
                        mydb.commit()
                        mycursor.close()
                    elif (Level == 'NFL'):
                        mycursor = mydb.cursor()
                        sql = "insert into relation_coach_job VALUES (%s,%s,%s,%s,0,(select TeamID from list_team where TeamName=%s),0)"
                        val = (CoachID, Year, RoleID, LevelID, Team)
                        mycursor.execute(sql, val)
                        mydb.commit()
                        mycursor.close()
                    elif (Level == 'High School'):
                        mycursor = mydb.cursor()
                        sql = "insert into relation_coach_job VALUES (%s,%s,%s,%s,0,0,0)"
                        val = (CoachID, Year, RoleID, LevelID)
                        mycursor.execute(sql, val)
                        mydb.commit()
                        mycursor.close()
                    elif ((Level == "NFL Europe")or(Level == "XFL")or(Level == "CFL")or(Level == "USFL")or(Level == "UFL")or(Level == "AAF")or(Level == "AFL")):
                        mycursor = mydb.cursor()
                        sql = "insert into relation_coach_job VALUES (%s,%s,%s,%s,0,0,%s)"
                        val = (CoachID, Year, RoleID, LevelID, TeamID)
                        mycursor.execute(sql, val)
                        mydb.commit()
                        mycursor.close()
                    else:
                        print(Year,Level,Team, Role,"NOT ENTERED!!!!")

    

    
#populate_pfr_url()
#populate_coach_college()
#populate_coach_entry()
#populate_coach_jobs()




# mycursor = mydb.cursor()
# mycursor.execute("select * from list_coach where CoachCollegeID is NULL")
# myresult = mycursor.fetchall()
# mycursor.close()
# mycursor = mydb.cursor()
# for x in myresult:
#     #Load html's plain data into a variable
#     plain_html_text = requests.get(x[9])
#     #parse the data
#     soup = BeautifulSoup(plain_html_text.text, "html.parser")
#     body = soup.body


#     trs = soup.find_all('table',{'class' : 'infobox vcard'})[0].tbody.find_all('tr')
#     time.sleep(5)
#     for tr in trs:
#         if ('College:' in tr.text):
#             CollegeCommonName = (tr.text).replace("College:", "")
#             CoachName = str(x[1])
#             print(CollegeCommonName, CoachName)
#                 sql = "UPDATE NFL.list_coach SET CoachBirthday = %s WHERE CoachID = %s"
#                 val = (Birth, CoachID)
#                 mycursor.execute(sql, val)
#                 mydb.commit()









# #URL to be scraped
# url_to_scrape = 'https://en.wikipedia.org/wiki/Adam_Zimmer'
# #Load html's plain data into a variable
# plain_html_text = requests.get(url_to_scrape)
# #parse the data
# soup = BeautifulSoup(plain_html_text.text, "html.parser")

# body = soup.body

# #FIND AGE
# Birth = soup.find_all('span',{'class' : 'bday'})[0].text
# BirthYear = int(Birth.split("-")[0])
# BirthMonth = int(Birth.split("-")[1])
# BirthDay = int(Birth.split("-")[2])
# Age = int(calculateAge(date(BirthYear, BirthMonth, BirthDay)))

# #FIND CURRENT POSITION
# CurrentPosition = "None"
# RetiredPlayer = False
# DraftPosition = ""
# PlayerName = soup.find_all('caption', {'class' : 'fn'})[0].text
# trs = soup.find_all('table',{'class' : 'infobox vcard'})[0].tbody.find_all('tr')
# for tr in trs:
#     if ('As player' in tr.text):
#         RetiredPlayer = True
#     if ('NFL Draft' in tr.text):
#         DraftPosition = tr.text
#         RetiredPlayer = True
#     for td in tr.find_all("td"):
#         for li in td.find_all("li"):
#             if ('present' in li.text):
#                 CurrentPosition = li.text 
# print("Name:",PlayerName)
# print("Age:",Age)
# print("Current Coaching Position:",CurrentPosition)
# print("Retired Player:",RetiredPlayer)
# if (RetiredPlayer):
#     print("Draft Position:",DraftPosition)


# #GET Player, Coach, Executive URL's
# url_to_scrape = 'https://www.pro-football-reference.com/search/search.fcgi?search=Jeff+Fisher'
# #Load html's plain data into a variable
# plain_html_text = requests.get(url_to_scrape)
# #parse the data
# soup = BeautifulSoup(plain_html_text.text, "html.parser")
# links=soup.body.find_all("div", {'class' : 'search-results'})[0].find_all("a")
# for link in links:
#     if '/players/' in link['href']:
#         PlayerURL = link['href']
#     if '/coaches/' in link['href']:
#         CoachURL = link['href']
#     if '/executives/' in link['href']:
#         ExecutiveURL = link['href']
# print(PlayerURL)
# print(CoachURL)
# print(ExecutiveURL)

# #Call PlayerURL
# url_to_scrape = 'https://www.pro-football-reference.com/'+PlayerURL
# #Load html's plain data into a variable
# plain_html_text = requests.get(url_to_scrape)
# #parse the data
# soup = BeautifulSoup(plain_html_text.text, "html.parser")
# tables = soup.find_all("table")
# #for table in tables:
#     #print(table)






    # #FIND Birthday
    # if (soup.find_all('span',{'class' : 'bday'}) == []):
    #     print("NONE!!!!!!!!!!!!!!!!!!!!!!!")
    # else:
    #     Birth = soup.find_all('span',{'class' : 'bday'})[0].text
    #     CoachID = int(x[0])
    #     print(Birth,CoachID)
    #     sql = "UPDATE NFL.list_coach SET CoachBirthday = %s WHERE CoachID = %s"
    #     val = (Birth, CoachID)
    #     mycursor.execute(sql, val)
    #     mydb.commit()


    # # #FIND CURRENT POSITION
    # # CurrentPosition = "None"
    # # RetiredPlayer = False
    # # DraftPosition = ""
    # # PlayerName = soup.find_all('caption', {'class' : 'fn'})[0].text
    # # trs = soup.find_all('table',{'class' : 'infobox vcard'})[0].tbody.find_all('tr')
    # # for tr in trs:
    # #     if ('As player' in tr.text):
    # #         RetiredPlayer = True
    # #     if ('NFL Draft' in tr.text):
    # #         DraftPosition = tr.text
    # #         RetiredPlayer = True
    # #     for td in tr.find_all("td"):
    # #         for li in td.find_all("li"):
    # #             if ('present' in li.text):
    # #                 CurrentPosition = li.text 
    # # print("Name:",PlayerName)
    # # print("Age:",Age)
    # # print("Current Coaching Position:",CurrentPosition)
    # # print("Retired Player:",RetiredPlayer)
    # # if (RetiredPlayer):
    # #     print("Draft Position:",DraftPosition)
