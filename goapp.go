package main
import (
	"encoding/json"
    "log"
	"database/sql"
	"os"
	"runtime"
	"html/template"
	"net/http"
	_ "github.com/go-sql-driver/mysql"
)


var db *sql.DB
var tmpl = template.Must(template.ParseGlob("Templates/*"))
var err error

//STRUCTS
type structdbconnection struct {
	DB_host	string
	DB_user	string
	DB_passwd	string
	DB_database	string
	DB_auth_plugin	string
	DB_driver	string
	DB_port	string
}

//RANDOM FUNCTIONS
//defalt error function
func checkerr(e error) {
    if e != nil {
		pc := make([]uintptr, 15)
    	n := runtime.Callers(2, pc)
    	frames := runtime.CallersFrames(pc[:n])
    	frame, _ := frames.Next()
    	log.Println(frame.File, frame.Line, frame.Function)
		panic(e)
    }
}
//LoadDBConfiguration function
func LoadDBConfiguration(file string) structdbconnection {
	//load DB configuration from config file
	var config structdbconnection
    configFile, err := os.Open(file)
    defer configFile.Close()
	checkerr(err)
    jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)
    return config
}

//TEMPLATE FUNCTIONS
func Index (rw http.ResponseWriter, req *http.Request){
	if req.Method == "GET" {
		tmpl.ExecuteTemplate(rw, "Index","")
	}
}

func main(){
	//Create DB connection
	dbconfig := LoadDBConfiguration("Excludes/db_connection.json")
	dbDriver := dbconfig.DB_driver
	dbUser := dbconfig.DB_user
	dbPass := dbconfig.DB_passwd
	dbName := dbconfig.DB_database
	dbServerIP := dbconfig.DB_host
	dbServerPort := dbconfig.DB_port
	db, err = sql.Open(dbDriver, dbUser+":"+dbPass+"@tcp("+dbServerIP+":"+dbServerPort+")/"+dbName)
	checkerr(err)



	mux := http.NewServeMux()
	mux.Handle("/", http.HandlerFunc(Index))
	

	//Non SSL -- go run goapp.go
	http.ListenAndServe(":8081", mux)


}
